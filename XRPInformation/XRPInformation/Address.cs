﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XRPInformation
{
    class Address
    {
        public int AccountId { get; set; }
        public string PublicAddress { get; set; }
    }
}
