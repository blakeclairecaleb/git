﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;

namespace XRPInformation
{
    public partial class Form1 : Form
    {

        DataReadWrite useDb = new DataReadWrite();
        AccountLines ac = new AccountLines();
        UploadCSV csv = new UploadCSV();

        public Form1()
        {
            InitializeComponent();

            dgvMain.DataSource = useDb.getTrustLines();
            //dgvAccounts.DataSource = useDb.getAccounts();
            dgvConOrClose.DataSource = useDb.getPossibleConsolidations();
            dgvZeroBalance.DataSource = useDb.getZeroBalance();
            dgvSpread.DataSource = useDb.getSpread();
        }

        private void btnAddCSV_Click(object sender, EventArgs e)
        {
            if (tbxCSVPath.Text != "")
            {
                try
                {
                    useDb.clearTrustlines();
                    useDb.clearAddresses();

                    useDb.insertAddressesFromCSV(tbxCSVPath.Text);

                    refreshFromAPI();

                    dgvAccounts.DataSource = useDb.getAccounts();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to successfully upload CSV - Please check file contents & path then try again.");
                }

            }
            else
            {
                MessageBox.Show("Please enter full filepath of CSV for upload");
            }
        }

        public void getAccountsFromAPI(object o, DoWorkEventArgs args)
        {
            //loop through all accounts in the database
            List<Address> addresses = useDb.GetAddresses();

            foreach (Address a in addresses)
            {
                ac.readAccountLinesFromAPI(a.AccountId, a.PublicAddress);
            }

            
        }
        private void OnRunWorkerCompleted(object o, RunWorkerCompletedEventArgs args)
        {
            pbxLoading.Hide();
            lblInfo.Hide();
        }

        private void refreshFromAPI()
        {
            lblInfo.Show();
            pbxLoading.Show();
            pbxLoading.Update();

            BackgroundWorker worker = new BackgroundWorker();
            try
            {
                worker.DoWork += getAccountsFromAPI;
                useDb.clearTrustlines();
                worker.RunWorkerCompleted += OnRunWorkerCompleted;
                worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving data from XRP Ledger - Network may be down, try again later.");
            }
        }

        private  void btnRefresh_Click(object sender, EventArgs e)
        {
            refreshFromAPI();
        }

        private void tbxSearchTokenName_TextChanged(object sender, EventArgs e)
        {
            //on text change search for token name except where the length is 0
            if(tbxSearchTokenName.TextLength > 0) 
            {
                dgvFilterTokens.DataSource = useDb.getDistinctToken(tbxSearchTokenName.Text);
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'xRPAccountsDataSet.addresses' table. You can move, or remove it, as needed.
            this.addressesTableAdapter.Fill(this.xRPAccountsDataSet.addresses);
           
        }

        private void tbcMain_MouseClick(object sender, MouseEventArgs e)
        {
            dgvMain.DataSource = useDb.getTrustLines();
        }

        private void tbxNicknameFilter_TextChanged(object sender, EventArgs e)
        {
            //on text change search for token name except where the length is 0
            if (tbxNicknameFilter.TextLength > 0)
            {
                dgvNicknameFilter.DataSource = useDb.getAccounts(tbxNicknameFilter.Text);
            }
        }

        private void tabConOrClose_Click(object sender, EventArgs e)
        {
            dgvConOrClose.DataSource = useDb.getPossibleConsolidations();
        }

        private void tabPage2_Click(object sender, EventArgs e)
        {
            dgvZeroBalance.DataSource = useDb.getZeroBalance();
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {
            dgvSpread.DataSource = useDb.getSpread();
        }

        private void dgvZeroBalance_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvZeroBalance.CurrentCell == dgvZeroBalance.CurrentRow.Cells["Trustline Name"])
            {
                dgvAccountsWithZero.DataSource = useDb.getAccountsWithZero((string)dgvZeroBalance.CurrentCell.Value);
            }
        }

        

        private void dgvZeroBalance_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgvZeroBalance.CurrentCell == dgvZeroBalance.CurrentRow.Cells["Trustline Name"])
            {
                dgvAccountsWithZero.DataSource = useDb.getAccountsWithZero((string)dgvZeroBalance.CurrentCell.Value);
            }
        }

        private void dgvFilterTokens_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //bring up all info for selected coin
            dgvTokensInAccounts.DataSource = useDb.getTokens((string)dgvFilterTokens.CurrentCell.Value);
            dgvMissingAccounts.DataSource = useDb.getMissingAccounts((string)dgvFilterTokens.CurrentCell.Value);

            lblSum.Text = useDb.getSumofTokens((string)dgvFilterTokens.CurrentCell.Value).ToString();
            lblLines.Text = useDb.getNumberofLines((string)dgvFilterTokens.CurrentCell.Value).ToString();
        }

        private void dgvTokensInAccounts_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            foreach (DataGridViewRow Myrow in dgvTokensInAccounts.Rows)
            {            //Here 2 cell is target value and 1 cell is Volume
                if (Convert.ToDecimal(Myrow.Cells[4].Value) == 0)// Or your condition 
                {
                    Myrow.DefaultCellStyle.BackColor = Color.LightSalmon;
                }
                else
                {
                    Myrow.DefaultCellStyle.BackColor = Color.LightGreen;
                }
            }
        }
    }
}
