﻿
namespace XRPInformation
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbcMain = new System.Windows.Forms.TabControl();
            this.tbpMain = new System.Windows.Forms.TabPage();
            this.dgvMain = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.dgvMissingAccounts = new System.Windows.Forms.DataGridView();
            this.lblLines = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblSum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvTokensInAccounts = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvFilterTokens = new System.Windows.Forms.DataGridView();
            this.tbxSearchTokenName = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tbxNicknameFilter = new System.Windows.Forms.TextBox();
            this.dgvNicknameFilter = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgvSpread = new System.Windows.Forms.DataGridView();
            this.tabConOrClose = new System.Windows.Forms.TabPage();
            this.dgvConOrClose = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.dgvAccountsWithZero = new System.Windows.Forms.DataGridView();
            this.dgvZeroBalance = new System.Windows.Forms.DataGridView();
            this.tbpAdmin = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAddCSV = new System.Windows.Forms.Button();
            this.tbxCSVPath = new System.Windows.Forms.TextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvAccounts = new System.Windows.Forms.DataGridView();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.publicaddressDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nicknameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.addressesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.xRPAccountsDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.xRPAccountsDataSet = new XRPInformation.XRPAccountsDataSet();
            this.addressesTableAdapter = new XRPInformation.XRPAccountsDataSetTableAdapters.addressesTableAdapter();
            this.xRPAccountsDataSetBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pbxLoading = new System.Windows.Forms.PictureBox();
            this.lblInfo = new System.Windows.Forms.Label();
            this.tbcMain.SuspendLayout();
            this.tbpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMissingAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTokensInAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilterTokens)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNicknameFilter)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSpread)).BeginInit();
            this.tabConOrClose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvConOrClose)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountsWithZero)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZeroBalance)).BeginInit();
            this.tbpAdmin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xRPAccountsDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xRPAccountsDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xRPAccountsDataSetBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLoading)).BeginInit();
            this.SuspendLayout();
            // 
            // tbcMain
            // 
            this.tbcMain.Controls.Add(this.tbpMain);
            this.tbcMain.Controls.Add(this.tabPage1);
            this.tbcMain.Controls.Add(this.tabPage3);
            this.tbcMain.Controls.Add(this.tabPage4);
            this.tbcMain.Controls.Add(this.tabConOrClose);
            this.tbcMain.Controls.Add(this.tabPage2);
            this.tbcMain.Controls.Add(this.tbpAdmin);
            this.tbcMain.Location = new System.Drawing.Point(10, 34);
            this.tbcMain.Name = "tbcMain";
            this.tbcMain.SelectedIndex = 0;
            this.tbcMain.Size = new System.Drawing.Size(1288, 579);
            this.tbcMain.TabIndex = 10;
            this.tbcMain.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tbcMain_MouseClick);
            // 
            // tbpMain
            // 
            this.tbpMain.Controls.Add(this.dgvMain);
            this.tbpMain.Location = new System.Drawing.Point(4, 22);
            this.tbpMain.Name = "tbpMain";
            this.tbpMain.Padding = new System.Windows.Forms.Padding(3);
            this.tbpMain.Size = new System.Drawing.Size(1280, 577);
            this.tbpMain.TabIndex = 0;
            this.tbpMain.Text = "Main";
            this.tbpMain.UseVisualStyleBackColor = true;
            // 
            // dgvMain
            // 
            this.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMain.Location = new System.Drawing.Point(5, 101);
            this.dgvMain.Name = "dgvMain";
            this.dgvMain.RowTemplate.Height = 25;
            this.dgvMain.Size = new System.Drawing.Size(1271, 472);
            this.dgvMain.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.dgvMissingAccounts);
            this.tabPage1.Controls.Add(this.lblLines);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.lblSum);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.dgvTokensInAccounts);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.dgvFilterTokens);
            this.tabPage1.Controls.Add(this.tbxSearchTokenName);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1280, 577);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Filter Token";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1084, 73);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 10;
            this.label10.Text = "Missing Accounts";
            // 
            // dgvMissingAccounts
            // 
            this.dgvMissingAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMissingAccounts.Location = new System.Drawing.Point(1084, 98);
            this.dgvMissingAccounts.Name = "dgvMissingAccounts";
            this.dgvMissingAccounts.Size = new System.Drawing.Size(193, 298);
            this.dgvMissingAccounts.TabIndex = 9;
            // 
            // lblLines
            // 
            this.lblLines.AutoSize = true;
            this.lblLines.Location = new System.Drawing.Point(911, 158);
            this.lblLines.Name = "lblLines";
            this.lblLines.Size = new System.Drawing.Size(13, 13);
            this.lblLines.TabIndex = 8;
            this.lblLines.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(814, 158);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Total Lines:";
            // 
            // lblSum
            // 
            this.lblSum.AutoSize = true;
            this.lblSum.Location = new System.Drawing.Point(911, 180);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(28, 13);
            this.lblSum.TabIndex = 6;
            this.lblSum.Text = "0.00";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(814, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Total Tokens:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(27, 61);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Click to Select";
            // 
            // dgvTokensInAccounts
            // 
            this.dgvTokensInAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTokensInAccounts.Location = new System.Drawing.Point(207, 77);
            this.dgvTokensInAccounts.Name = "dgvTokensInAccounts";
            this.dgvTokensInAccounts.Size = new System.Drawing.Size(591, 494);
            this.dgvTokensInAccounts.TabIndex = 3;
            this.dgvTokensInAccounts.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgvTokensInAccounts_CellFormatting);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(112, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Search";
            // 
            // dgvFilterTokens
            // 
            this.dgvFilterTokens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilterTokens.Location = new System.Drawing.Point(21, 77);
            this.dgvFilterTokens.Name = "dgvFilterTokens";
            this.dgvFilterTokens.RowTemplate.Height = 25;
            this.dgvFilterTokens.Size = new System.Drawing.Size(180, 494);
            this.dgvFilterTokens.TabIndex = 1;
            this.dgvFilterTokens.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFilterTokens_CellClick);
            // 
            // tbxSearchTokenName
            // 
            this.tbxSearchTokenName.Location = new System.Drawing.Point(159, 23);
            this.tbxSearchTokenName.Name = "tbxSearchTokenName";
            this.tbxSearchTokenName.Size = new System.Drawing.Size(144, 20);
            this.tbxSearchTokenName.TabIndex = 0;
            this.tbxSearchTokenName.TextChanged += new System.EventHandler(this.tbxSearchTokenName_TextChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tbxNicknameFilter);
            this.tabPage3.Controls.Add(this.dgvNicknameFilter);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1280, 577);
            this.tabPage3.TabIndex = 5;
            this.tabPage3.Text = "Filter Account Nickname";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tbxNicknameFilter
            // 
            this.tbxNicknameFilter.Location = new System.Drawing.Point(22, 19);
            this.tbxNicknameFilter.Name = "tbxNicknameFilter";
            this.tbxNicknameFilter.Size = new System.Drawing.Size(213, 20);
            this.tbxNicknameFilter.TabIndex = 1;
            this.tbxNicknameFilter.TextChanged += new System.EventHandler(this.tbxNicknameFilter_TextChanged);
            // 
            // dgvNicknameFilter
            // 
            this.dgvNicknameFilter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNicknameFilter.Location = new System.Drawing.Point(22, 56);
            this.dgvNicknameFilter.Name = "dgvNicknameFilter";
            this.dgvNicknameFilter.Size = new System.Drawing.Size(896, 396);
            this.dgvNicknameFilter.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgvSpread);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1280, 577);
            this.tabPage4.TabIndex = 6;
            this.tabPage4.Text = "Spread";
            this.tabPage4.UseVisualStyleBackColor = true;
            this.tabPage4.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // dgvSpread
            // 
            this.dgvSpread.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSpread.Location = new System.Drawing.Point(22, 26);
            this.dgvSpread.Name = "dgvSpread";
            this.dgvSpread.Size = new System.Drawing.Size(869, 436);
            this.dgvSpread.TabIndex = 0;
            // 
            // tabConOrClose
            // 
            this.tabConOrClose.Controls.Add(this.dgvConOrClose);
            this.tabConOrClose.Location = new System.Drawing.Point(4, 22);
            this.tabConOrClose.Name = "tabConOrClose";
            this.tabConOrClose.Padding = new System.Windows.Forms.Padding(3);
            this.tabConOrClose.Size = new System.Drawing.Size(1280, 577);
            this.tabConOrClose.TabIndex = 3;
            this.tabConOrClose.Text = "Consolidate or Close";
            this.tabConOrClose.UseVisualStyleBackColor = true;
            this.tabConOrClose.Click += new System.EventHandler(this.tabConOrClose_Click);
            // 
            // dgvConOrClose
            // 
            this.dgvConOrClose.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvConOrClose.Location = new System.Drawing.Point(14, 65);
            this.dgvConOrClose.Name = "dgvConOrClose";
            this.dgvConOrClose.RowTemplate.Height = 25;
            this.dgvConOrClose.Size = new System.Drawing.Size(765, 387);
            this.dgvConOrClose.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.dgvAccountsWithZero);
            this.tabPage2.Controls.Add(this.dgvZeroBalance);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1280, 577);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "Zero Balance Tokens";
            this.tabPage2.UseVisualStyleBackColor = true;
            this.tabPage2.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(312, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Click on a Trustline name to see which accounts they are set for.";
            // 
            // dgvAccountsWithZero
            // 
            this.dgvAccountsWithZero.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccountsWithZero.Location = new System.Drawing.Point(351, 50);
            this.dgvAccountsWithZero.Name = "dgvAccountsWithZero";
            this.dgvAccountsWithZero.Size = new System.Drawing.Size(472, 398);
            this.dgvAccountsWithZero.TabIndex = 1;
            // 
            // dgvZeroBalance
            // 
            this.dgvZeroBalance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvZeroBalance.Location = new System.Drawing.Point(29, 50);
            this.dgvZeroBalance.Name = "dgvZeroBalance";
            this.dgvZeroBalance.Size = new System.Drawing.Size(266, 398);
            this.dgvZeroBalance.TabIndex = 0;
            this.dgvZeroBalance.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvZeroBalance_CellClick);
            this.dgvZeroBalance.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvZeroBalance_CellContentDoubleClick);
            // 
            // tbpAdmin
            // 
            this.tbpAdmin.Controls.Add(this.pbxLoading);
            this.tbpAdmin.Controls.Add(this.label4);
            this.tbpAdmin.Controls.Add(this.label3);
            this.tbpAdmin.Controls.Add(this.label2);
            this.tbpAdmin.Controls.Add(this.btnAddCSV);
            this.tbpAdmin.Controls.Add(this.tbxCSVPath);
            this.tbpAdmin.Controls.Add(this.btnRefresh);
            this.tbpAdmin.Controls.Add(this.label1);
            this.tbpAdmin.Controls.Add(this.dgvAccounts);
            this.tbpAdmin.Location = new System.Drawing.Point(4, 22);
            this.tbpAdmin.Name = "tbpAdmin";
            this.tbpAdmin.Padding = new System.Windows.Forms.Padding(3);
            this.tbpAdmin.Size = new System.Drawing.Size(1280, 553);
            this.tbpAdmin.TabIndex = 1;
            this.tbpAdmin.Text = "Admin";
            this.tbpAdmin.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(824, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(239, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Click Refresh to pull latest data down from XRPL ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(692, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Full Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(625, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(561, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Upload CSV file with headings addresses, nicknames then rows beneath it - Ensurin" +
    "g items are separated by a comma";
            // 
            // btnAddCSV
            // 
            this.btnAddCSV.Location = new System.Drawing.Point(827, 113);
            this.btnAddCSV.Name = "btnAddCSV";
            this.btnAddCSV.Size = new System.Drawing.Size(236, 39);
            this.btnAddCSV.TabIndex = 10;
            this.btnAddCSV.Text = "Upload CSV";
            this.btnAddCSV.UseVisualStyleBackColor = true;
            this.btnAddCSV.Click += new System.EventHandler(this.btnAddCSV_Click);
            // 
            // tbxCSVPath
            // 
            this.tbxCSVPath.Location = new System.Drawing.Point(749, 77);
            this.tbxCSVPath.Name = "tbxCSVPath";
            this.tbxCSVPath.Size = new System.Drawing.Size(381, 20);
            this.tbxCSVPath.TabIndex = 9;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(827, 200);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(236, 37);
            this.btnRefresh.TabIndex = 6;
            this.btnRefresh.Text = "Refresh Lines";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 15F);
            this.label1.Location = new System.Drawing.Point(15, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(162, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Current Accounts";
            // 
            // dgvAccounts
            // 
            this.dgvAccounts.AutoGenerateColumns = false;
            this.dgvAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAccounts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.publicaddressDataGridViewTextBoxColumn,
            this.nicknameDataGridViewTextBoxColumn});
            this.dgvAccounts.DataSource = this.addressesBindingSource;
            this.dgvAccounts.Location = new System.Drawing.Point(20, 51);
            this.dgvAccounts.Name = "dgvAccounts";
            this.dgvAccounts.RowTemplate.Height = 25;
            this.dgvAccounts.Size = new System.Drawing.Size(548, 523);
            this.dgvAccounts.TabIndex = 0;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
            this.idDataGridViewTextBoxColumn.HeaderText = "Id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // publicaddressDataGridViewTextBoxColumn
            // 
            this.publicaddressDataGridViewTextBoxColumn.DataPropertyName = "public_address";
            this.publicaddressDataGridViewTextBoxColumn.HeaderText = "public_address";
            this.publicaddressDataGridViewTextBoxColumn.Name = "publicaddressDataGridViewTextBoxColumn";
            // 
            // nicknameDataGridViewTextBoxColumn
            // 
            this.nicknameDataGridViewTextBoxColumn.DataPropertyName = "nickname";
            this.nicknameDataGridViewTextBoxColumn.HeaderText = "nickname";
            this.nicknameDataGridViewTextBoxColumn.Name = "nicknameDataGridViewTextBoxColumn";
            // 
            // addressesBindingSource
            // 
            this.addressesBindingSource.DataMember = "addresses";
            this.addressesBindingSource.DataSource = this.xRPAccountsDataSetBindingSource;
            // 
            // xRPAccountsDataSetBindingSource
            // 
            this.xRPAccountsDataSetBindingSource.DataSource = this.xRPAccountsDataSet;
            this.xRPAccountsDataSetBindingSource.Position = 0;
            // 
            // xRPAccountsDataSet
            // 
            this.xRPAccountsDataSet.DataSetName = "XRPAccountsDataSet";
            this.xRPAccountsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // addressesTableAdapter
            // 
            this.addressesTableAdapter.ClearBeforeFill = true;
            // 
            // xRPAccountsDataSetBindingSource1
            // 
            this.xRPAccountsDataSetBindingSource1.DataSource = this.xRPAccountsDataSet;
            this.xRPAccountsDataSetBindingSource1.Position = 0;
            // 
            // pbxLoading
            // 
            this.pbxLoading.BackColor = System.Drawing.Color.White;
            this.pbxLoading.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pbxLoading.Image = global::XRPInformation.Properties.Resources.loading;
            this.pbxLoading.InitialImage = global::XRPInformation.Properties.Resources.loading;
            this.pbxLoading.Location = new System.Drawing.Point(827, 276);
            this.pbxLoading.Name = "pbxLoading";
            this.pbxLoading.Size = new System.Drawing.Size(236, 224);
            this.pbxLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxLoading.TabIndex = 14;
            this.pbxLoading.TabStop = false;
            this.pbxLoading.Visible = false;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.ForeColor = System.Drawing.Color.Red;
            this.lblInfo.Location = new System.Drawing.Point(12, 2);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(282, 29);
            this.lblInfo.TabIndex = 11;
            this.lblInfo.Text = "Loading Data From API";
            this.lblInfo.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1309, 624);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.tbcMain);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tbcMain.ResumeLayout(false);
            this.tbpMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMain)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMissingAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTokensInAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilterTokens)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNicknameFilter)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSpread)).EndInit();
            this.tabConOrClose.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvConOrClose)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccountsWithZero)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvZeroBalance)).EndInit();
            this.tbpAdmin.ResumeLayout(false);
            this.tbpAdmin.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xRPAccountsDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xRPAccountsDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xRPAccountsDataSetBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxLoading)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tbcMain;
        private System.Windows.Forms.TabPage tbpMain;
        private System.Windows.Forms.TabPage tbpAdmin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvAccounts;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.DataGridView dgvMain;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataGridView dgvFilterTokens;
        private System.Windows.Forms.TextBox tbxSearchTokenName;
        private System.Windows.Forms.TabPage tabConOrClose;
        private System.Windows.Forms.DataGridView dgvConOrClose;
        private System.Windows.Forms.BindingSource xRPAccountsDataSetBindingSource;
        private XRPAccountsDataSet xRPAccountsDataSet;
        private System.Windows.Forms.BindingSource addressesBindingSource;
        private XRPAccountsDataSetTableAdapters.addressesTableAdapter addressesTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn publicaddressDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nicknameDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource xRPAccountsDataSetBindingSource1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dgvZeroBalance;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox tbxNicknameFilter;
        private System.Windows.Forms.DataGridView dgvNicknameFilter;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dgvSpread;
        private System.Windows.Forms.DataGridView dgvAccountsWithZero;
        private System.Windows.Forms.Button btnAddCSV;
        private System.Windows.Forms.TextBox tbxCSVPath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvTokensInAccounts;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label lblLines;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dgvMissingAccounts;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pbxLoading;
        private System.Windows.Forms.Label lblInfo;
    }
}

