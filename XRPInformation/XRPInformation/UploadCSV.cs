﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XRPInformation
{
    class UploadCSV
    {
        public string PublicAddress { get; set; }
        public string Nickname { get; set; }

        

        public List<UploadCSV> readCSV(string path)
        {
            var csvTable = new DataTable();
            using (var csvReader = new CsvReader(new StreamReader(System.IO.File.OpenRead(path)), true))
            {
                csvTable.Load(csvReader);
            }

            List<UploadCSV> rows = new List<UploadCSV>();

            for (int i = 0; i < csvTable.Rows.Count; i++)
            {
                rows.Add(new UploadCSV { PublicAddress = csvTable.Rows[i][0].ToString(), Nickname = csvTable.Rows[i][1].ToString() });
            }

            return rows;
        }


    }
}
