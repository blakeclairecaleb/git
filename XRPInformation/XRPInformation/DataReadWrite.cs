﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;


namespace XRPInformation
{
    class DataReadWrite
    {

        //SqlConnection local = new SqlConnection("Data Source=.;Integrated Security=True;AttachDbFilename=|DataDirectory|XRPAccounts.mdf");


        SqlConnection local = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\XRPAccounts.mdf;Integrated Security = True");
        UploadCSV upload = new UploadCSV();

        internal void insertAddress(String address, String nickname)
        {
            //opens database connection
            local.Open();

            //defines query which inserts values from material object into appropriate columns
            string insertquery = "INSERT INTO addresses (public_address, nickname)VALUES('";
            insertquery += address + "','";
            insertquery += nickname + "')";

            //passes in values to new command, the update query and the data connection
            SqlCommand querycommand = new SqlCommand(insertquery, local);

            //executes the insert query
            querycommand.ExecuteNonQuery();

            //closes the database connection
            local.Close();
        }

        internal void clearAddresses()
        {
            local.Open();
            //defines query which inserts values from material object into appropriate columns
            string insertquery = "TRUNCATE TABLE addresses";

            //passes in values to new command, the update query and the data connection
            SqlCommand querycommand = new SqlCommand(insertquery, local);

            //executes the insert query
            querycommand.ExecuteNonQuery();

            //closes the database connection
            local.Close();
        }



        internal void insertAddressesFromCSV(string path)
        {
            List<UploadCSV> addresses =  upload.readCSV(path);
            foreach (UploadCSV u in addresses)
            {
                local.Open();

          

                //defines query which inserts values from material object into appropriate columns
                string insertquery = "INSERT INTO addresses (public_address, nickname)VALUES('";
                insertquery += u.PublicAddress + "','";
                insertquery += u.Nickname + "')";

                //passes in values to new command, the update query and the data connection
                SqlCommand querycommand = new SqlCommand(insertquery, local);

                //executes the insert query
                querycommand.ExecuteNonQuery();

                //closes the database connection
                local.Close();
            }
        }

        internal List<Address> GetAddresses()
        {
            List<Address> addresses = new List<Address>();

            local.Open();

            SqlCommand querycommand = new SqlCommand("Select Id, public_address from addresses", local);
            SqlDataReader queryreader = querycommand.ExecuteReader();

            while(queryreader.Read())
            {
                Address current = new Address();
                current.AccountId = (int)queryreader[0];
                current.PublicAddress = queryreader[1] as string;

                addresses.Add(current);
            }

            local.Close();

            return addresses;
        }



        internal DataTable getAccounts()
        {
            string sql = "select * from addresses";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getTrustLines()
        {
            string sql = "select public_address, nickname, trustline, trustline_name, balance from addresses a join trustlines t on a.id = t.addressesId";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getZeroBalance()
        {
            string sql = "select trustline_name as [TrustLine Name], count(distinct addressesId) as [Number of Accounts]  from trustlines t group by trustline_name having sum(balance) = 0";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getAccountsWithZero(string trustlineName)
        {
            string sql = "select trustline_name as [Trustline Name], nickname as [Account Nickname]  from addresses a join trustlines t on a.id = t.addressesId where trustline_name like '%" + trustlineName + "%'";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getTokens(String searchValue)
        {
            string sql = "select public_address, nickname, trustline, trustline_name, isnull(balance, 0) as balance from addresses a join trustlines t on a.id = t.addressesId where trustline_name = '" + searchValue + "'";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getMissingAccounts(String searchValue)
        {
            string sql = "select nickname from addresses a where id not in (select addressesId from trustlines where trustline_name ='" + searchValue + "')";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getDistinctToken(String searchValue)
        {
            string sql = "select distinct trustline_name from addresses a join trustlines t on a.id = t.addressesId where trustline_name like '%" + searchValue + "%'";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal decimal getSumofTokens(String searchValue)
        {
            decimal i = 0;
            local.Open();
             
            SqlCommand sql = new SqlCommand("select SUM(balance) as balance from trustlines where trustline_name = '" + searchValue + "'", local);
            
               i = (decimal)sql.ExecuteScalar();
            

            local.Close();

            return i;
        }

        internal int getNumberofLines(String searchValue)
        {
            int i = 0;
            local.Open();

            SqlCommand sql = new SqlCommand("select count(*) from trustlines where trustline_name = '" + searchValue + "'", local);

            i = (int)sql.ExecuteScalar();


            local.Close();

            return i;
        }

        internal DataTable getAccounts(String searchValue)
        {
            string sql = "select public_address, nickname, trustline, trustline_name, balance from addresses a join trustlines t on a.id = t.addressesId where nickname like '%" + searchValue + "%'";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getSpread()
        {
            string sql = "select public_address, nickname, x.trustline, trustline_name, balance from(select distinct trustline from trustlines t where t.balance > 0 and not exists (select 1 from trustlines t2 where t2.trustline = t.trustline and t2.addressesId <> t.addressesId and t2.balance = 0) and exists (select 1 from trustlines t2 where t2.trustline = t.trustline and t2.addressesId <> t.addressesId and t2.balance > 0)) x join trustlines t on t.trustline = x.trustline join addresses a on a.id = t.addressesId order by x.trustline";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }

        internal DataTable getPossibleConsolidations()
        {
            string sql = "select public_address, nickname, x.trustline, trustline_name, balance from(select distinct trustline from trustlines t where t.balance > 0 and exists (select 1 from trustlines t2 where t2.trustline = t.trustline and t2.addressesId <> t.addressesId and t2.balance = 0)) x join trustlines t on t.trustline = x.trustline join addresses a on a.id = t.addressesId order by x.trustline";
            SqlDataAdapter adapter = new SqlDataAdapter(sql, local);
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            local.Open();
            adapter.Fill(dt);
            ds.Tables.Add(dt);
            local.Close();

            return dt;
        }


        internal void clearTrustlines()
        {
            local.Open();

            //defines query which inserts job object properties into appropriate columns of the job table
            string insertquery = "TRUNCATE TABLE trustlines";

            //passes in values to new command, the insert query and the data connection
            SqlCommand querycommand = new SqlCommand(insertquery, local);

            //executes the query
            querycommand.ExecuteNonQuery();

            //closes the database connection
            local.Close();
        }

        internal int getAddressID(string address)
        {
            int id = 0;

            //opens database connection
            local.Open();

            //defines query which inserts values from material object into appropriate columns
            string insertquery = "select id from addresses where public_address = '" + address + "'";

            //passes in values to new command, the update query and the data connection
            SqlCommand querycommand = new SqlCommand(insertquery, local);

            //executes the insert query
            id = Convert.ToInt32(querycommand.ExecuteScalar());

            //closes the database connection
            local.Close();

            return id;
        }

        internal void insertTrustline(int addressId, string trustLine, decimal balance, string currency)
        {
            local.Open();

            //defines query which inserts job object properties into appropriate columns of the job table
            string insertquery = "INSERT INTO trustlines (addressesId, trustline, balance, trustline_name) VALUES('";
            insertquery += addressId + "','";
            insertquery += trustLine + "','";
            insertquery += balance + "','";
            insertquery += currency + "')";

            //passes in values to new command, the insert query and the data connection
            SqlCommand querycommand = new SqlCommand(insertquery, local);

            //executes the query
            querycommand.ExecuteNonQuery();

            //closes the database connection
            local.Close();
        }

    }
}
