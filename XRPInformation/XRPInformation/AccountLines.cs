﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Runtime.Serialization.Json;
using RippleDotNet;
using RippleDotNet.Requests.Account;
using RippleDotNet.Model.Account;

namespace XRPInformation
{
    public  class AccountLines
    {
        public static string HexStringToBytes(string hexString)
        {
            if (hexString.Length > 3)
            {
                //hexString = hexString.Replace("0", "");
                 

                if (hexString == null)
                    throw new ArgumentNullException("hexString");
                if (hexString.Length % 2 != 0)
                    hexString = hexString + "0";
                var bytes = new byte[hexString.Length / 2];
                for (int i = 0; i < bytes.Length; i++)
                {
                    string currentHex = hexString.Substring(i * 2, 2);
                    bytes[i] = Convert.ToByte(currentHex, 16);
                }

                string output = Encoding.GetEncoding("UTF-8").GetString(bytes);
                return output;

            }
            else return hexString;
        }



        public async void readAccountLinesFromAPI(int id, string address)
        {
            DataReadWrite db = new DataReadWrite();

            AccountLinesRequest alr = new AccountLinesRequest(address);
            alr.Limit = 200;

            IRippleClient client = new RippleClient("wss://xrpl.ws");
            client.Connect();

            var accountInfo = await client.AccountLines(alr);
            client.Disconnect();

            List<TrustLine> t = accountInfo.TrustLines;

            foreach (TrustLine lines in t)
            {
                //Account
                //Database function to insert new row reading lines.Account  & lines.Balance;

                decimal bal = decimal.Parse(lines.Balance);

                db.insertTrustline(id, lines.Account, bal, HexStringToBytes(lines.Currency));

            }
        }

	}
}
